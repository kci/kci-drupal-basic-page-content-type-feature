<?php
/**
 * @file
 * basic_page_content_type.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function basic_page_content_type_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create page content'.
  $permissions['create page content'] = array(
    'name' => 'create page content',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any page content'.
  $permissions['delete any page content'] = array(
    'name' => 'delete any page content',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own page content'.
  $permissions['delete own page content'] = array(
    'name' => 'delete own page content',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any page content'.
  $permissions['edit any page content'] = array(
    'name' => 'edit any page content',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own page content'.
  $permissions['edit own page content'] = array(
    'name' => 'edit own page content',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'node',
  );

  // Exported permission: 'publish button publish any page'.
  $permissions['publish button publish any page'] = array(
    'name' => 'publish button publish any page',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own page'.
  $permissions['publish button publish own page'] = array(
    'name' => 'publish button publish own page',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any page'.
  $permissions['publish button unpublish any page'] = array(
    'name' => 'publish button unpublish any page',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own page'.
  $permissions['publish button unpublish own page'] = array(
    'name' => 'publish button unpublish own page',
    'roles' => array(
      'developer' => 'developer',
    ),
    'module' => 'publish_button',
  );

  return $permissions;
}
