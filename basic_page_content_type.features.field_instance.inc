<?php
/**
 * @file
 * basic_page_content_type.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function basic_page_content_type_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-page-field_page_content'.
  $field_instances['node-page-field_page_content'] = array(
    'bundle' => 'page',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_page_content',
    'label' => 'Page Content',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'button',
      'allowed_bundles' => array(
        'file_attachments' => 'file_attachments',
        'image' => 'image',
        'text_content' => 'text_content',
      ),
      'bundle_weights' => array(
        'file_attachments' => -4,
        'image' => -5,
        'text_content' => -6,
      ),
      'default_edit_mode' => 'open',
      'title' => 'Block',
      'title_multiple' => 'Blocks',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Page Content');

  return $field_instances;
}
